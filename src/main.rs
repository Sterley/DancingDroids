use rand::Rng;


fn main() {

    //Les differentes commandes du jeu
    let mut commandes = [""; 6];
    commandes[0] = "Z";
    commandes[1] = "D";
    commandes[2] = "Q";
    commandes[3] = "S";
    commandes[4] = "F";
    commandes[5] = "X";
    //Les differentes commandes du jeu

        //Les differentes commandes du jeu
        let mut commandes2 = [""; 8];
        commandes2[0] = "Z";
        commandes2[1] = "F";
        commandes2[2] = "D";
        commandes2[3] = "F";
        commandes2[4] = "Q";
        commandes2[5] = "F";
        commandes2[6] = "S";
        commandes2[7] = "F";
        //Les differentes commandes du jeu


    //Les differentes orientations
    let mut orientations = [""; 4];
    orientations[0] = "Nord";
    orientations[1] = "Sud";
    orientations[2] = "Est";
    orientations[3] = "Ouest";
    //Les differentes orientations

    //Structures pour les robots
    struct Robot {
        id: i32,
        x: i32,
        y: i32, 
        o: String,
        img: String,
    };
    //Structures pour les robots

    // Mon premier Robot
    let mut robot1 = Robot {
        id: 1, 
        x: 3, 
        y: 9,
        o: (&orientations[0]).to_string(),
        img: format!("⇧"),
    };
    // Mon premier Robot

    // Mon 2eme Robot
    let mut robot2 = Robot {
        id: 2, 
        x: 2, 
        y: 5,
        o: (&orientations[0]).to_string(),
        img: format!("↑"),
    };
    // Mon 2eme Robot

    // Mon 3eme Robot
    let mut robot3 = Robot {
        id: 3, 
        x: 2, 
        y: 4,
        o: (&orientations[0]).to_string(),
        img: format!("↑"),
    };
    // Mon 3eme Robot

    // Mon 4eme Robot
    let mut robot4 = Robot {
        id: 4, 
        x: 1, 
        y: 1,
        o: (&orientations[0]).to_string(),
        img: format!("↑"),
    };
    // Mon 4eme Robot


    // Mon terrain de jeu
    let width = 5;
    let height = 11;

    let mut terrain: Vec<Vec<String>> = vec![vec![String::from(""); width]; height];

    for i in 0..height {
        for j in 0..width {
            let s = format!(" ");
            terrain[i][j] = s;
        }
    }
    terrain[robot1.y as usize ][robot1.x as usize ] = robot1.img.to_string();
    terrain[robot2.y as usize ][robot2.x as usize ] = robot2.img.to_string();
    terrain[robot3.y as usize ][robot3.x as usize ] = robot3.img.to_string();
    terrain[robot4.y as usize ][robot4.x as usize ] = robot4.img.to_string();
    // Mon terrain de jeu


    //Affichage Initiale
    println!("|");
    println!("Position Initiale");
    println!("|");
    for i in 0..height {
        println!("{:?}", terrain[i]);
    }
    println!("|");
    println!("Robot 1 (id : {}, x : {}, y : {}, o : {})", robot1.id, robot1.x, robot1.y, robot1.o);
    println!("Robot 2 (id : {}, x : {}, y : {}, o : {})", robot2.id, robot2.x, robot2.y, robot2.o);
    println!("Robot 3 (id : {}, x : {}, y : {}, o : {})", robot3.id, robot3.x, robot3.y, robot3.o);
    println!("Robot 4 (id : {}, x : {}, y : {}, o : {})", robot4.id, robot4.x, robot4.y, robot4.o);
    //Affichage Initiale


    let mut bool_val = true;

    while bool_val {

        use std::io::{stdin,stdout,Write};
        let mut s=String::new();
        println!("|");
        println!("COMMANDES :");
        println!("Z : se tourner vers le Nord");
        println!("Q : se tourner vers l'Ouest");
        println!("S : se tourner vers le Sud");
        println!("D : se tourner vers l'Est");
        println!("F : Avancer dans la direction choisie");
        println!("X : Pour Quitter le Jeu");
        println!("|");
        print!("Entrez une instruction: ");
        let _=stdout().flush();
        stdin().read_line(&mut s).expect("Did not enter a correct string");
        if let Some('\n')=s.chars().next_back() {
            s.pop();
        }
        if let Some('\r')=s.chars().next_back() {
            s.pop();
        }
        println!("|");
        println!("Instruction: {}",s);
        println!("|");



//Traitement des instructions pour le premier robot

        if s == (&commandes[0]).to_string() {
            
            robot1.o = orientations[0].to_string();

            robot1.img = format!("⇧");
            
        } 
        else if s == (&commandes[1]).to_string() {
            
            robot1.o = orientations[2].to_string();

            robot1.img = format!("⇨");
            
        }
        else if s == (&commandes[2]).to_string() {
           
            robot1.o = orientations[3].to_string();

            robot1.img = format!("⇦");
            
        }
        else if s == (&commandes[3]).to_string() {
            
            robot1.o = orientations[1].to_string();

            robot1.img = format!("⇩");
            
        }
        else if s == (&commandes[4]).to_string() {

            if robot1.o == orientations[0].to_string() {

                robot1.y = &robot1.y-1;
                if robot1.y < 0 {
                    robot1.y = 0;
                    println!("Robot1: 'Je suis Bloque ! On change de direction' ");
                }
                
            }

            else if robot1.o == orientations[1].to_string() {

                robot1.y = &robot1.y+1;
                if robot1.y >= height as i32 {
                    robot1.y = (height-1) as i32;
                    println!("Robot1: 'Je suis Bloque ! On change de direction' ");
                }
                
            }    

            else if robot1.o == orientations[2].to_string() {

                robot1.x = &robot1.x+1;
                if robot1.x >= width as i32 {
                    robot1.x = (width-1) as i32;
                    println!("Robot1: 'Je suis Bloque ! On change de direction' ");
                }
                

            }

            else if robot1.o == orientations[3].to_string() {

                robot1.x = &robot1.x-1;
                if robot1.x < 0 {
                    robot1.x = 0;
                    println!("Robot1: 'Je suis Bloque ! On change de direction' ");
                }

            }            

        }
//Traitement des instructions pour le premier robot




//Quitter Le jeu
        else if s == (&commandes[5]).to_string() {
            
            bool_val = false;
            println!("Vous avez quitte le jeu !");           
        }
//Quitter Le jeu

// Mauvaises commandes
        else {
            println!("Utilisez les commandes du jeu et en Majuscule !");
        }
// Mauvaises commandes


        let mut rng2 = rand::thread_rng();
        let com_rng2 = rng2.gen_range(0, 8);

        let s2 = commandes2[com_rng2];


//Traitement des instructions pour 2eme robot

        if s2 == (&commandes2[0]).to_string() {
            
            robot2.o = orientations[0].to_string();

            robot2.img = format!("↑");
            
        } 
        else if s2 == (&commandes2[2]).to_string() {
            
            robot2.o = orientations[2].to_string();

            robot2.img = format!("→");
            
        }
        else if s2 == (&commandes2[4]).to_string() {
           
            robot2.o = orientations[3].to_string();

            robot2.img = format!("←");
            
        }
        else if s2 == (&commandes2[6]).to_string() {
            
            robot2.o = orientations[1].to_string();

            robot2.img = format!("↓");
            
        }
        else {

            if robot2.o == orientations[0].to_string() {

                robot2.y = &robot2.y-1;
                if robot2.y < 0 {
                    robot2.y = 0;

                    robot2.o = orientations[1].to_string();
                    robot2.img = format!("↓");
                }
                
            }

            else if robot2.o == orientations[1].to_string() {

                robot2.y = &robot2.y+1;
                if robot2.y >= height as i32 {
                    robot2.y = (height-1) as i32;

                    robot2.o = orientations[0].to_string();
                    robot2.img = format!("↑");
                }
                
            }    

            else if robot2.o == orientations[2].to_string() {

                robot2.x = &robot2.x+1;
                if robot2.x >= width as i32 {
                    robot2.x = (width-1) as i32;

                    robot2.o = orientations[3].to_string();
                    robot2.img = format!("←");
                }
                

            }

            else if robot2.o == orientations[3].to_string() {

                robot2.x = &robot2.x-1;
                if robot2.x < 0 {
                    robot2.x = 0;

                    robot2.o = orientations[2].to_string();
                    robot2.img = format!("→");
                }

            }            

        }
//Traitement des instructions pour le 2eme robot



        let mut rng3 = rand::thread_rng();
        let com_rng3 = rng3.gen_range(0, 8);

        let s3 = commandes2[com_rng3];


//Traitement des instructions pour 3eme robot

        if s3 == (&commandes2[0]).to_string() {
            
            robot3.o = orientations[0].to_string();

            robot3.img = format!("↑");
            
        } 
        else if s3 == (&commandes2[2]).to_string() {
            
            robot3.o = orientations[2].to_string();

            robot3.img = format!("→");
            
        }
        else if s3 == (&commandes2[4]).to_string() {
           
            robot3.o = orientations[3].to_string();

            robot3.img = format!("←");
            
        }
        else if s3 == (&commandes2[6]).to_string() {
            
            robot3.o = orientations[1].to_string();

            robot3.img = format!("↓");
            
        }
        else {

            if robot3.o == orientations[0].to_string() {

                robot3.y = &robot3.y-1;
                if robot3.y < 0 {
                    robot3.y = 0;

                    robot3.o = orientations[1].to_string();
                    robot3.img = format!("↓");
                }
                
            }

            else if robot3.o == orientations[1].to_string() {

                robot3.y = &robot3.y+1;
                if robot3.y >= height as i32 {
                    robot3.y = (height-1) as i32;

                    robot3.o = orientations[0].to_string();
                    robot3.img = format!("↑");
                }
                
            }    

            else if robot3.o == orientations[2].to_string() {

                robot3.x = &robot3.x+1;
                if robot3.x >= width as i32 {
                    robot3.x = (width-1) as i32;

                    robot3.o = orientations[3].to_string();
                    robot3.img = format!("←");
                }
                

            }

            else if robot3.o == orientations[3].to_string() {

                robot3.x = &robot3.x-1;
                if robot3.x < 0 {
                    robot3.x = 0;

                    robot3.o = orientations[2].to_string();
                    robot3.img = format!("→");
                }

            }            

        }
//Traitement des instructions pour le 3eme robot




        let mut rng4 = rand::thread_rng();
        let com_rng4 = rng4.gen_range(0, 8);

        let s4 = commandes2[com_rng4];


//Traitement des instructions pour 3eme robot

        if s4 == (&commandes2[0]).to_string() {
            
            robot4.o = orientations[0].to_string();

            robot4.img = format!("↑");
            
        } 
        else if s4 == (&commandes2[2]).to_string() {
            
            robot4.o = orientations[2].to_string();

            robot4.img = format!("→");
            
        }
        else if s4 == (&commandes2[4]).to_string() {
           
            robot4.o = orientations[3].to_string();

            robot4.img = format!("←");
            
        }
        else if s4 == (&commandes2[6]).to_string() {
            
            robot4.o = orientations[1].to_string();

            robot4.img = format!("↓");
            
        }
        else {

            if robot4.o == orientations[0].to_string() {

                robot4.y = &robot4.y-1;
                if robot4.y < 0 {
                    robot4.y = 0;

                    robot4.o = orientations[1].to_string();
                    robot4.img = format!("↓");
                }
                
            }

            else if robot4.o == orientations[1].to_string() {

                robot4.y = &robot4.y+1;
                if robot4.y >= height as i32 {
                    robot4.y = (height-1) as i32;

                    robot4.o = orientations[0].to_string();
                    robot4.img = format!("↑");
                }
                
            }    

            else if robot4.o == orientations[2].to_string() {

                robot4.x = &robot4.x+1;
                if robot4.x >= width as i32 {
                    robot4.x = (width-1) as i32;

                    robot4.o = orientations[3].to_string();
                    robot4.img = format!("←");
                }
                

            }

            else if robot4.o == orientations[3].to_string() {

                robot4.x = &robot4.x-1;
                if robot4.x < 0 {
                    robot4.x = 0;

                    robot4.o = orientations[2].to_string();
                    robot4.img = format!("→");
                }

            }            

        }
//Traitement des instructions pour le 3eme robot






// Affichage du tableau apres traitement des instructions
        for i in 0..height {
            for j in 0..width {
                let s = format!(" ");
                terrain[i][j] = s;
            }
        }

        terrain[robot1.y as usize ][robot1.x as usize ] = robot1.img.to_string();
        terrain[robot2.y as usize ][robot2.x as usize ] = robot2.img.to_string();
        terrain[robot3.y as usize ][robot3.x as usize ] = robot3.img.to_string();
        terrain[robot4.y as usize ][robot4.x as usize ] = robot4.img.to_string();

        //Gestion des collisions

        if (robot1.x == robot2.x && robot1.y ==robot2.y) || (robot1.x == robot3.x && robot1.y ==robot3.y) || (robot1.x == robot4.x && robot1.y ==robot4.y) {
            terrain[robot1.y as usize ][robot1.x as usize ] = "X".to_string();
            println!("|");
            println!("Collision entre les Robots ! GAME OVER");
            bool_val = false;
        }

        //Gestion des collisions

        for i in 0..height {
            println!("{:?}", terrain[i]);
        }
        
        println!("|");
        println!("Robot 1 (id : {}, x : {}, y : {}, o : {})", robot1.id, robot1.x, robot1.y, robot1.o);
        println!("Robot 2 (id : {}, x : {}, y : {}, o : {})", robot2.id, robot2.x, robot2.y, robot2.o);
        println!("Robot 3 (id : {}, x : {}, y : {}, o : {})", robot3.id, robot3.x, robot3.y, robot3.o);
        println!("Robot 4 (id : {}, x : {}, y : {}, o : {})", robot4.id, robot4.x, robot4.y, robot4.o);
// Affichage du tableau apres traitement des instructions



        
        
    }
   

}
